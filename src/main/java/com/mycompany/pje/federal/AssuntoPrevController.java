/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;

/**
 *
 * @author root2
 */
public class AssuntoPrevController implements Initializable
{

    @FXML
    TableView<Headers> tablePrev;
    @FXML
    TableColumn<Headers, String> colNum;
    @FXML
    TableColumn<Headers, String> colAssunto;
    @FXML
    JFXTextField AssuntoTxt;

    // create a text input dialog 
    TextInputDialog td = new TextInputDialog();

    @FXML
    public void handle()
    {

        // show the text input dialog 
        //td.showAndWait();
        Optional<String> result = td.showAndWait();
        if (result.isPresent())
        {
            Headers headers = new Headers();
            headers.setAssunto(result.get().toUpperCase());
            Banco banco = new Banco();
            banco.alterarConfig(headers, tablePrev.getSelectionModel().getSelectedItem().getAssunto());
            List<Headers> cabecalho = new ArrayList<>();
            try
            {
                Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
                PreparedStatement stmt = connection.prepareStatement("select * from header order by headerDoc ");
                ResultSet resultSet = stmt.executeQuery();
                int i = 1;
                while (resultSet.next())
                {
                    Headers key = new Headers();
                    key.setAssunto(resultSet.getString("headerDoc"));
                    key.setNumero(i + "");
                    cabecalho.add(key);
                    i++;
                }
                colAssunto.setCellValueFactory(new PropertyValueFactory<Headers, String>("Assunto"));
                colNum.setCellValueFactory(new PropertyValueFactory<Headers, String>("Numero"));
                ObservableList<Headers> genericos = FXCollections.observableArrayList(cabecalho);
                tablePrev.setItems(genericos);

            }
            catch (Exception ex)
            {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Informação");
                alert.setHeaderText("Erro ao inserir registro");
                alert.setContentText(ex.getCause().initCause(ex).toString());
                alert.showAndWait();
            }
        }
    }

    @FXML
    public void limpar()
    {
        AssuntoTxt.clear();
    }

    @FXML
    void RetornaMenu(ActionEvent event)
    {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        }
        catch (IOException ex)
        {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        stage.setTitle("Mark-II");

    }

    @FXML
    void help(ActionEvent event)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informação");
        alert.setHeaderText("Cadastro de Cabeçalho padrão do documento");
        alert.setContentText("Nesta janela você irá cadastrar o cabeçalho padrão das paginas em que o robô precisa ler"
                + "\nExemplo:VARA FEDERAL DE JUIZADO ESPECIAL CÍVEL");
        alert.showAndWait();
    }

    @FXML
    @SuppressWarnings("ConvertToTryWithResources")
    private boolean inserir()
    {
        String radical = AssuntoTxt.getText().toUpperCase();
        Banco banco = new Banco();
        Headers chave = new Headers();
        if ((radical.equals(null)))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        }
        else
        {
            chave.setAssunto(AssuntoTxt.getText().toUpperCase().trim().replace("'", "").replace("´", "").replace("\"", ""));
            banco.inserirConfig(chave);
        }
        List<Headers> cabecalho = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            PreparedStatement stmt = connection.prepareStatement("select * from header order by headerDoc");
            ResultSet resultSet = stmt.executeQuery();
            int i = 1;
            while (resultSet.next())
            {
                Headers key = new Headers();
                key.setAssunto(resultSet.getString("headerDoc"));
                key.setNumero(i + "");
                cabecalho.add(key);
                i++;
            }
            colAssunto.setCellValueFactory(new PropertyValueFactory<Headers, String>("Assunto"));
            colNum.setCellValueFactory(new PropertyValueFactory<Headers, String>("Numero"));
            ObservableList<Headers> genericos = FXCollections.observableArrayList(cabecalho);
            tablePrev.setItems(genericos);

        }
        catch (Exception ex)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Informação");
            alert.setHeaderText("Erro ao inserir registro");
            alert.setContentText(ex.getCause().initCause(ex).toString());
            alert.showAndWait();
        }
        return true;
    }

    @FXML
    public void selecionar()
    {
        AssuntoTxt.clear();
        AssuntoTxt.setText(tablePrev.getSelectionModel().getSelectedItem().getAssunto());
    }

    @FXML
    @SuppressWarnings("ConvertToTryWithResources")
    public void excluir()
    {
        int op;
        op = JOptionPane.showConfirmDialog(null, "Deseja Realmente Apagar este Registro?", "Apagar Registro", JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION)
        {
            Banco banco = new Banco();
            Headers chave = new Headers();
            chave.setAssunto(AssuntoTxt.getText());
            //chave.setRadical(Radical.getText().trim().replace("'","").replace("´", ""));
            //chave.setComplemento(Complemento.getText().trim().replace("'","").replace("´", ""));
            AssuntoTxt.clear();
            banco.excluirConfig(chave);
            List<Headers> cabecalho = new ArrayList<>();
            try
            {
                Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
                PreparedStatement stmt = connection.prepareStatement("select * from header order by headerDoc ");
                ResultSet resultSet = stmt.executeQuery();
                int i = 1;
                while (resultSet.next())
                {
                    Headers key = new Headers();
                    key.setAssunto(resultSet.getString("headerDoc"));
                    key.setNumero(i + "");
                    cabecalho.add(key);
                    i++;
                }
                colAssunto.setCellValueFactory(new PropertyValueFactory<Headers, String>("Assunto"));
                colNum.setCellValueFactory(new PropertyValueFactory<Headers, String>("Numero"));
                ObservableList<Headers> genericos = FXCollections.observableArrayList(cabecalho);
                tablePrev.setItems(genericos);

            }
            catch (SQLException ex)
            {

                Logger.getLogger(AssuntoPrevController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        List<Headers> cabecalho = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            PreparedStatement stmt = connection.prepareStatement("select * from header order by headerDoc ");
            ResultSet resultSet = stmt.executeQuery();
            int i = 1;
            while (resultSet.next())
            {
                Headers key = new Headers();
                key.setAssunto(resultSet.getString("headerDoc"));
                key.setNumero("" + i);
                cabecalho.add(key);
                i++;
            }
            colAssunto.setCellValueFactory(new PropertyValueFactory<Headers, String>("Assunto"));
            colNum.setCellValueFactory(new PropertyValueFactory<Headers, String>("Numero"));
            ObservableList<Headers> genericos = FXCollections.observableArrayList(cabecalho);
            tablePrev.setItems(genericos);

        }
        catch (SQLException ex)
        {
            Logger.getLogger(AssuntoPrevController.class.getName()).log(Level.SEVERE, null, ex);
        }
        td.setTitle("Editar Registro");
        td.setHeaderText("Insira a alteração do registro");
    }

}

package com.mycompany.pje.federal;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author root
 */
public class MateriasPrev
{

    public boolean Assuntos(String capa)
    {
        List<String> assuntos = new ArrayList();

        assuntos.add("APOSENTADORIA ESPECIAL".toUpperCase());
        assuntos.add("ASSISTÊNCIA SOCIAL".toUpperCase());
        assuntos.add("IDOSO".toUpperCase());
        assuntos.add("AUXÍLIO-DOENÇA".toUpperCase());
        assuntos.add("APOSENTADORIA POR IDADE".toUpperCase());
        assuntos.add("APOSENTADORIA POR INVALIDEZ".toUpperCase());
        assuntos.add("APOSENTADORIA POR TEMPO DE CONTRIBUIÇÃO".toUpperCase());
        assuntos.add("BENEFÍCIO ASSISTENCIAL".toUpperCase());
        assuntos.add("Concessão".toUpperCase());
        assuntos.add("CONVERSÃO".toUpperCase());
        assuntos.add("DEFICIENTE".toUpperCase());
        assuntos.add("PARCELAS DE BENEFÍCIO NÃO PAGAS".toUpperCase());
        assuntos.add("PENSÃO POR MORTE".toUpperCase());
        assuntos.add("RESTABELECIMENTO");
        assuntos.add("RMI - RENDA MENSAL INICIAL".toUpperCase());
        assuntos.add("RURAL".toUpperCase());
        assuntos.add("SALÁRIO-MATERNIDADE".toUpperCase());
        assuntos.add("Seguro-Desemprego".toUpperCase());
        assuntos.add("URBANA".toUpperCase());
        assuntos.add("AUXÍLIO-RECLUSÃO".toUpperCase());
        assuntos.add("SEGURO-DEFESO".toUpperCase());
        assuntos.add("DIREITO PREVIDENCIÁRIO".toUpperCase());
        assuntos.add("DATA DE INÍCIO DE BENEFÍCIO".toUpperCase());
        assuntos.add("CÁLCULO DO BENEFÍCIO".toUpperCase());
        assuntos.add("RMI SEM INCIDÊNCIA DE TETO LIMITADOR".toUpperCase());
        assuntos.add("APLICAÇÃO DE COEFICIENTE DE CÁLCULO".toUpperCase());
        assuntos.add("ALTERAÇÃO DO COEFICIENTE DE CÁLCULO".toUpperCase());
        assuntos.add("DESCONTOS DOS BENEFÍCIOS".toUpperCase());
        assuntos.add("AVERBAÇÃO/CÔMPUTO DO TEMPO DE SERVIÇO".toUpperCase());
        assuntos.add("ABONO DA LEI 8.178/91".toUpperCase());
        assuntos.add("ART. 26 DA LEI 8.870/1994".toUpperCase());
        assuntos.add("ACIDENTE DE TRÂNSITO".toUpperCase());
        assuntos.add("INSS".toUpperCase());
        assuntos.add("INSTITUTO NACIONAL".toUpperCase());
        assuntos.add("PREVIDENCIA SOCIAL".toUpperCase());
        try
        {
            for (int i = 0; i <= assuntos.size() - 1; i++)
            {
                if (capa.contains(assuntos.get(i)))
                {
                    return true;
                }
            }
        }
        catch (IndexOutOfBoundsException e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
            System.exit(0);
        }
        return false;
    }

    public boolean materia(String Capa)
    {

        if (Assuntos(Capa))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

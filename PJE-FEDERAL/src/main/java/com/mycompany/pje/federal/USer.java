/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author carlos.luan / eduardo.luiz
 */
public class USer
{

    private String Login =""; //"25510487291";
    private String Senha =""; //"Sapiens1994";
    private String PalavraChave = "";

    //Altera palavra chave da busca
    public void setPalavraChave(String chave)
    {
        PalavraChave = chave;
    }

    public String getPalavraChave()
    {
        return PalavraChave;
    }

    public String getSenha()
    {
        return Senha;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public void setSenha(String Senha) {
        this.Senha = Senha;
    }

    public String getLogin()
    {
        return Login;
    }

    public void exec_login(WebDriver driver,USer Usuario) throws InterruptedException
    {
        // recebe o elemento para colocar o login
        WebElement login = driver.findElement(By.xpath("//input[@id='cpffield-1017-inputEl']"));
        // envia o elemento para enviar os dados do login
        Thread.sleep(1000);
        login.click();
        login.sendKeys(Usuario.getLogin());
        // recebe o elemento para colocar a senha
        WebElement senha = driver.findElement(By.xpath("//td[@id='textfield-1018-bodyEl']/input"));
        Thread.sleep(1000);
        senha.click();
        senha.sendKeys(Usuario.getSenha());
        // envia o elemento para enviar os dados da senha
        Thread.sleep(1000);
        WebElement Click = driver.findElement(By.xpath("//span[@id='button-1019-btnInnerEl']"));
        Click.click();
    }
    
}
